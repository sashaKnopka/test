module xsolla

go 1.17

require gorm.io/gorm v1.22.5

require github.com/mattn/go-sqlite3 v1.14.9 // indirect

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	gorm.io/driver/sqlite v1.2.6
)
