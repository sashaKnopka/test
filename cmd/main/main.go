package main

import (
	"context"
	"encoding/csv"
	"errors"
	"io"
	"os"
	"strings"

	"xsolla/internal/user/model"

	"golang.org/x/sync/errgroup"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

const errDBDuplicate = "UNIQUE constraint failed"

func main() {
	errs, _ := errgroup.WithContext(context.Background())
	duplChan := make(chan []string, 100)
	dbChan := make(chan []string, 100)

	errs.Go(func() error { return readFile(dbChan) })
	errs.Go(func() error { return writeDuplToFile(duplChan) })
	errs.Go(func() error { return saveUserToDB(dbChan, duplChan) })
	errs.Wait()
}

func readFile(dbChan chan<- []string) error {
	defer close(dbChan)
	userFile, err := os.Open("data/users.csv")
	if err != nil {
		return err
	}
	defer userFile.Close()

	reader := csv.NewReader(userFile)
	for {
		row, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			// TODO не смогли прочитать строку, залогировать
			continue
		}
		dbChan <- row
	}
	return nil
}

func writeDuplToFile(duplChan <-chan []string) error {
	duplFile, err := os.Create("data/dupl.csv")
	if err != nil {
		return err
	}
	defer duplFile.Close()

	writer := csv.NewWriter(duplFile)
	batch := 100
	num := 0
	for {
		user, ok := <-duplChan
		if !ok {
			break
		}
		err = writer.Write(user)
		if err != nil {
			return err
		}
		num++
		if num == batch {
			writer.Flush()
			num = 0
		}
	}
	if num > 0 {
		writer.Flush()
	}
	return nil
}

// TODO ПОДУМАТЬ: вставлять не по одной строке в БД, но тогда как обрабатывать дубли?
func saveUserToDB(dbChan <-chan []string, duplChan chan<- []string) error {
	defer close(duplChan)
	db, err := gorm.Open(sqlite.Open("data/user.db"), &gorm.Config{})
	if err != nil {
		return errors.New("failed to connect database")
	}
	db.AutoMigrate(&model.User{})
	for {
		row, ok := <-dbChan
		if !ok {
			break
		}
		user, err := model.NewUser(row[0], row[1], row[2], row[3], row[4])
		if err != nil {
			// TODO залогировать ошибку
			continue
		}
		tx := db.Create(user)
		if tx.Error != nil {
			switch {
			case strings.Contains(tx.Error.Error(), errDBDuplicate):
				duplChan <- row
			default:
				// TODO залогировать ошибку вставки
				continue
			}
		}
	}
	return nil
}
