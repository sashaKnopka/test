package model

import (
	"errors"
	"regexp"
)

var (
	birthdayRegexp = regexp.MustCompile(`^\d{2}\.\d{2}\.\d{4}$`)
	phoneRegexp    = regexp.MustCompile(`^79\d{9}$`)
)

type User struct {
	ID       string `csv:"id"`
	Name     string `csv:"name"`
	Surname  string `csv:"surname"`
	Birthday string `csv:"birthday"`
	Phone    string `csv:"phone"`
}

func NewUser(id, name, surname, birthday, phone string) (*User, error) {
	if err := validate(id, birthday, phone); err != nil {
		return nil, err
	}
	user := User{
		ID:       id,
		Name:     name,
		Surname:  surname,
		Birthday: birthday,
		Phone:    phone,
	}
	return &user, nil
}

func validate(id, birthday, phone string) error {
	if id == "" {
		return errors.New("id is invalid")
	}
	if birthday != "" && !birthdayRegexp.MatchString(birthday) {
		return errors.New("birthday is invalid")
	}
	if phone != "" && !phoneRegexp.MatchString(phone) {
		return errors.New("phone is invalid")
	}
	return nil
}
