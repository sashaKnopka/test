/*
Вывести список сотрудников, не имеющих назначенного руководителя, работающего в том-же отделе
*/
SELECT
	e.*
FROM
	public.employee e
LEFT JOIN public.employee chief ON
	e.chief_id = chief.id
	AND e.department_id = chief.department_id
WHERE
	chief.id IS NULL;