/*
Вывести список сотрудников, получающих заработную плату большую чем у непосредственного руководителя
*/
SELECT
	e.*
FROM
	public.employee e
INNER JOIN public.employee chief ON
	e.chief_id = chief.id
	AND e.salary > chief.salary;