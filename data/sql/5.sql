/*
Найти список ID отделов с максимальной суммарной зарплатой сотрудников
*/
WITH sum_salary AS
  (
    SELECT
        e.department_id,
        sum(e.salary) salary
    FROM
        public.employee e
    GROUP BY
        e.department_id
  )
SELECT
	department_id
FROM
	sum_salary s
WHERE
	s.salary = (
        SELECT
            max(salary)
        FROM
            sum_salary
	);