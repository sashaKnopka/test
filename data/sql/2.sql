/*
Вывести список сотрудников, получающих максимальную заработную плату в своем отделе
*/
SELECT
	e.*
FROM
	public.employee e
INNER JOIN (
	SELECT
		department_id,
		max(salary) AS max_salary
	FROM
		public.employee e
	GROUP BY
		department_id
	) e2
ON
	e.department_id = e2.department_id
	AND e.salary = e2.max_salary;