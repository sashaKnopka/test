/*
Вывести список ID отделов, количество сотрудников в которых не превышает 3 человек
*/
SELECT
	e.department_id
FROM
	public.employee e
GROUP BY
	e.department_id
HAVING
	count(e.department_id) <= 3;